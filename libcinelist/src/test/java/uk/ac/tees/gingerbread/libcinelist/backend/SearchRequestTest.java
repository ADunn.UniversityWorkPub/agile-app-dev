package uk.ac.tees.gingerbread.libcinelist.backend;

import org.junit.Test;
import uk.ac.tees.gingerbread.libcinelist.dto.search.SearchResult;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SearchRequestTest
{
    @Test
    public void byPostcodeHasValue() throws IOException
    {
        SearchResult result = SearchRequest.byPostcode("LU12HN");
        assertNotNull(result);
    }

    @Test
    public void byPostcodeFieldHasValue() throws IOException
    {
        SearchResult result = SearchRequest.byPostcode("LU12HN");
        assertEquals("LU12HN", result.getPostcode());
    }

    @Test
    public void byLocationHasValue() throws IOException
    {
        SearchResult result = SearchRequest.byLocation("St. Albans");
        assertNotNull(result);
    }

    @Test
    public void byLocationFieldHasValue() throws IOException
    {
        SearchResult result = SearchRequest.byLocation("St. Albans");
        assertEquals("AL11DW", result.getPostcode());
    }

    @Test
    public void byCoordinatesHasValue() throws IOException
    {
        SearchResult result = SearchRequest.byCoordinates(50.7200, -1.8800);
        assertNotNull(result);
    }

    @Test
    public void byCoordinatesFieldHasValue() throws IOException
    {
        SearchResult result = SearchRequest.byCoordinates(50.7200, -1.8800);
        assertEquals("BH26EG", result.getPostcode());
    }
}
