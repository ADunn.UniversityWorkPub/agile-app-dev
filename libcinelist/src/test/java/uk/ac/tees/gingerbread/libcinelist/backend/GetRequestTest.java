package uk.ac.tees.gingerbread.libcinelist.backend;

import org.junit.Test;
import uk.ac.tees.gingerbread.libcinelist.dto.get.CinemaDetails;
import uk.ac.tees.gingerbread.libcinelist.dto.get.ManyResult;
import uk.ac.tees.gingerbread.libcinelist.dto.get.TimesResult;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class GetRequestTest
{
    @Test
    public void detailsHasValue() throws IOException
    {
        CinemaDetails details = GetRequest.getDetails(7530);
        assertNotNull(details);
    }

    @Test
    public void detailsFieldHasValue() throws IOException
    {
        CinemaDetails details = GetRequest.getDetails(7530);
        assertEquals("Cineworld Luton", details.getName());
    }

    @Test
    public void timesHasValue() throws IOException
    {
        TimesResult result = GetRequest.getTimes(7530, 0);
        assertNotNull(result);
    }

    @Test
    public void timesFieldHasValue() throws IOException
    {
        TimesResult result = GetRequest.getTimes(7530, 0);
        assertEquals("ok", result.getStatus());
    }

    @Test
    public void manyHasValue() throws IOException
    {
        ManyResult result = GetRequest.getTimesMany(0, 7530, 7517, 10681);
        assertNotNull(result);
    }

    @Test
    public void manyFieldHasValue() throws IOException
    {
        ManyResult result = GetRequest.getTimesMany(0, 7530, 7517, 10681);
        assertEquals("ok", result.getStatus());
    }
}
