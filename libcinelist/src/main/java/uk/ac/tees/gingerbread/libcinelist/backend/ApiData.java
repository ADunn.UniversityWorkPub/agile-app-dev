package uk.ac.tees.gingerbread.libcinelist.backend;

public class ApiData
{
    private static final String API_URL = "https://api.cinelist.co.uk/";

    public static String getApiUrl()
    {
        return API_URL;
    }
}
