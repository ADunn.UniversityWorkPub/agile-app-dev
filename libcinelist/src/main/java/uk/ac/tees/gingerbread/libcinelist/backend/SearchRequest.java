package uk.ac.tees.gingerbread.libcinelist.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import uk.ac.tees.gingerbread.libcinelist.dto.search.SearchResult;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

import static java.net.URLEncoder.encode;

public class SearchRequest
{
    /**
     * Search for cinemas using a UK postcode
     *
     * @param postcode Postcode to base search around
     * @return A url for the request
     * @throws MalformedURLException Invalid url
     */
    private static URL byPostcodeUrl(String postcode) throws MalformedURLException
    {
        String url = ApiData.getApiUrl() + "/search/cinemas/postcode/";

        try
        {
            url += encode(postcode, "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }

        return new URL(url);
    }

    /**
     * Search for cinemas by town, city
     *
     * @param location Town or city
     * @return A url for the request
     * @throws MalformedURLException Invalid url
     */
    private static URL byLocationUrl(String location) throws MalformedURLException
    {
        String url = ApiData.getApiUrl() + "/search/cinemas/location/";

        try
        {
            url += encode(location, "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }

        return new URL(url);
    }

    /**
     * Search for cinemas by lat/long
     *
     * @param latitude  lat
     * @param longitude long
     * @return A url for the request
     * @throws MalformedURLException Invalid url
     */
    private static URL byCoordinatesUrl(double latitude, double longitude) throws MalformedURLException
    {
        String url = ApiData.getApiUrl() + "/search/cinemas/coordinates/";

        url += Double.toString(latitude) + "/";
        url += Double.toString(longitude);

        return new URL(url);
    }

    /**
     * Request a list of cinemas near a postcode
     *
     * @param postcode The postcode to search with
     * @return A list of cinemas
     * @throws IOException A json parsing issue
     */
    public static SearchResult byPostcode(String postcode) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(HttpRequest.requestJson(byPostcodeUrl(postcode)), SearchResult.class);
    }

    /**
     * Request a list of cinemas near a postcode
     *
     * @param location The postcode to search with
     * @return A list of cinemas
     * @throws IOException A json parsing issue
     */
    public static SearchResult byLocation(String location) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(HttpRequest.requestJson(byLocationUrl(location)), SearchResult.class);
    }

    /**
     * Request a list of cinemas near a postcode
     *
     * @param latitude  lat
     * @param longitude long
     * @return A list of cinemas
     * @throws IOException A json parsing issue
     */
    public static SearchResult byCoordinates(double latitude, double longitude) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(HttpRequest.requestJson(byCoordinatesUrl(latitude, longitude)), SearchResult.class);
    }
}
