package uk.ac.tees.gingerbread.libcinelist.dto.get;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status",
        "results"
})
public class ManyResult
{

    @JsonProperty("status")
    private String status;
    @JsonProperty("results")
    private List<Result> results = null;

    @JsonProperty("status")
    public String getStatus()
    {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status)
    {
        this.status = status;
    }

    @JsonProperty("results")
    public List<Result> getResults()
    {
        return results;
    }

    @JsonProperty("results")
    public void setResults(List<Result> results)
    {
        this.results = results;
    }

}
