package uk.ac.tees.gingerbread.libcinelist;

import uk.ac.tees.gingerbread.libcinelist.backend.GetRequest;
import uk.ac.tees.gingerbread.libcinelist.backend.SearchRequest;
import uk.ac.tees.gingerbread.libcinelist.dto.get.CinemaDetails;
import uk.ac.tees.gingerbread.libcinelist.dto.get.ManyResult;
import uk.ac.tees.gingerbread.libcinelist.dto.get.TimesResult;
import uk.ac.tees.gingerbread.libcinelist.dto.search.SearchResult;

import java.io.IOException;

/**
 * The stable api to Cinelist
 */
public class Cinelist
{
    public static SearchResult getCinemasByPostcode(String postcode) throws IOException
    {
        return SearchRequest.byPostcode(postcode);
    }

    public static SearchResult getCinemasByLatLong(double lat, double lng) throws IOException
    {
        return SearchRequest.byCoordinates(lat, lng);
    }

    public static CinemaDetails getCinemaDetails(int venueId) throws IOException
    {
        return GetRequest.getDetails(venueId);
    }

    public static ManyResult getManyShowings(int dayOffset, int... venueIds) throws IOException
    {
        return GetRequest.getTimesMany(dayOffset, venueIds);
    }

    public static TimesResult getShowings(int dayOffset, int venueId) throws IOException
    {
        return GetRequest.getTimes(venueId, dayOffset);
    }
}
