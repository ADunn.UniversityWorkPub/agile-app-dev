package uk.ac.tees.gingerbread.libcinelist.dto.search;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "postcode",
        "cinemas"
})
public class SearchResult
{

    @JsonProperty("postcode")
    private String postcode;
    @JsonProperty("cinemas")
    private List<Cinema> cinemas = null;

    @JsonProperty("postcode")
    public String getPostcode()
    {
        return postcode;
    }

    @JsonProperty("postcode")
    public void setPostcode(String postcode)
    {
        this.postcode = postcode;
    }

    @JsonProperty("cinemas")
    public List<Cinema> getCinemas()
    {
        return cinemas;
    }

    @JsonProperty("cinemas")
    public void setCinemas(List<Cinema> cinemas)
    {
        this.cinemas = cinemas;
    }

}
