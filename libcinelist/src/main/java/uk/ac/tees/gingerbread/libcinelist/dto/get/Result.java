package uk.ac.tees.gingerbread.libcinelist.dto.get;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "cinema",
        "listings"
})
public class Result
{

    @JsonProperty("cinema")
    private String cinema;
    @JsonProperty("listings")
    private List<Listing> listings = null;

    @JsonProperty("cinema")
    public String getCinema()
    {
        return cinema;
    }

    @JsonProperty("cinema")
    public void setCinema(String cinema)
    {
        this.cinema = cinema;
    }

    @JsonProperty("listings")
    public List<Listing> getListings()
    {
        return listings;
    }

    @JsonProperty("listings")
    public void setListings(List<Listing> listings)
    {
        this.listings = listings;
    }

}
