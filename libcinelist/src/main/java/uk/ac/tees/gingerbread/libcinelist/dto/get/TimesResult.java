package uk.ac.tees.gingerbread.libcinelist.dto.get;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status",
        "listings"
})
public class TimesResult
{

    @JsonProperty("status")
    private String status;
    @JsonProperty("listings")
    private List<Listing> listings = null;

    @JsonProperty("status")
    public String getStatus()
    {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status)
    {
        this.status = status;
    }

    @JsonProperty("listings")
    public List<Listing> getListings()
    {
        return listings;
    }

    @JsonProperty("listings")
    public void setListings(List<Listing> listings)
    {
        this.listings = listings;
    }

}
