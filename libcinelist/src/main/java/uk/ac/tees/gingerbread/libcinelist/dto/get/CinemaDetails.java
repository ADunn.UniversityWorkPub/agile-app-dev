package uk.ac.tees.gingerbread.libcinelist.dto.get;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "address1",
        "towncity",
        "show_towncity",
        "postcode",
        "website",
        "phone",
        "distance",
        "lat",
        "lon"
})
public class CinemaDetails
{

    @JsonProperty("name")
    private String name;
    @JsonProperty("address1")
    private String address1;
    @JsonProperty("towncity")
    private String towncity;
    @JsonProperty("show_towncity")
    private Boolean showTowncity;
    @JsonProperty("postcode")
    private String postcode;
    @JsonProperty("website")
    private String website;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("distance")
    private String distance;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lon")
    private String lon;

    @JsonProperty("name")
    public String getName()
    {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name)
    {
        this.name = name;
    }

    @JsonProperty("address1")
    public String getAddress1()
    {
        return address1;
    }

    @JsonProperty("address1")
    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    @JsonProperty("towncity")
    public String getTowncity()
    {
        return towncity;
    }

    @JsonProperty("towncity")
    public void setTowncity(String towncity)
    {
        this.towncity = towncity;
    }

    @JsonProperty("show_towncity")
    public Boolean getShowTowncity()
    {
        return showTowncity;
    }

    @JsonProperty("show_towncity")
    public void setShowTowncity(Boolean showTowncity)
    {
        this.showTowncity = showTowncity;
    }

    @JsonProperty("postcode")
    public String getPostcode()
    {
        return postcode;
    }

    @JsonProperty("postcode")
    public void setPostcode(String postcode)
    {
        this.postcode = postcode;
    }

    @JsonProperty("website")
    public String getWebsite()
    {
        return website;
    }

    @JsonProperty("website")
    public void setWebsite(String website)
    {
        this.website = website;
    }

    @JsonProperty("phone")
    public String getPhone()
    {
        return phone;
    }

    @JsonProperty("phone")
    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    @JsonProperty("distance")
    public String getDistance()
    {
        return distance;
    }

    @JsonProperty("distance")
    public void setDistance(String distance)
    {
        this.distance = distance;
    }

    @JsonProperty("lat")
    public String getLat()
    {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat)
    {
        this.lat = lat;
    }

    @JsonProperty("lon")
    public String getLon()
    {
        return lon;
    }

    @JsonProperty("lon")
    public void setLon(String lon)
    {
        this.lon = lon;
    }

}
