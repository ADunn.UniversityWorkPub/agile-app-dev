package uk.ac.tees.gingerbread.libcinelist.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import uk.ac.tees.gingerbread.libcinelist.dto.get.CinemaDetails;
import uk.ac.tees.gingerbread.libcinelist.dto.get.ManyResult;
import uk.ac.tees.gingerbread.libcinelist.dto.get.TimesResult;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class GetRequest
{
    /**
     * Url for venue details
     *
     * @param venueId The id of the venue
     * @return A url for the request
     */
    private static URL getDetailsUrl(int venueId)
    {
        String url = ApiData.getApiUrl() + "/get/cinema/";
        url += Integer.toString(venueId);

        try
        {
            return new URL(url);
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Url for fetching show times
     *
     * @param venueId   The id of the cinema
     * @param dayOffset Must be >= 0, how many days into the future to get times for
     * @return A url for the request
     */
    private static URL getTimesUrl(int venueId, int dayOffset)
    {
        String url = ApiData.getApiUrl() + "/get/times/cinema/";
        url += Integer.toString(venueId);
        url += "?day=" + Integer.toString(dayOffset);

        try
        {
            return new URL(url);
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Url for fetching show times
     *
     * @param venueIds  A vararg of venue ids
     * @param dayOffset Must be >= 0, how many days into the future to get times for
     * @return A url for the request
     */
    private static URL getTimesManyUrl(int dayOffset, int... venueIds)
    {
        StringBuilder url = new StringBuilder(ApiData.getApiUrl() + "/get/times/many/");

        for (int i = 0; i < venueIds.length - 1; i++)
        {
            url.append(Integer.toString(venueIds[i])).append(",");
        }

        //Append last w/o trailing comma
        url.append(venueIds[venueIds.length - 1]);
        url.append("?day=").append(Integer.toString(dayOffset));

        try
        {
            return new URL(url.toString());
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Fetches details about a cinema
     *
     * @param venueId The cinema's id
     * @return The cinema's details
     * @throws IOException JSON parsing issue
     */
    public static CinemaDetails getDetails(int venueId) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(HttpRequest.requestJson(getDetailsUrl(venueId)), CinemaDetails.class);
    }

    /**
     * Fetches film listings for a cinema
     *
     * @param venueId The cinema's id
     * @param days    Must be >= 0, how many days into the future to get times for
     * @return A showings for all films at the cinema
     * @throws IOException JSON parsing issue
     */
    public static TimesResult getTimes(int venueId, int days) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(HttpRequest.requestJson(getTimesUrl(venueId, days)), TimesResult.class);
    }

    /**
     * Fetches film listings for a cinema
     *
     * @param venueIds The list of cinema ids
     * @param days     Must be >= 0, how many days into the future to get times for
     * @return A showings for all films at the cinema
     * @throws IOException JSON parsing issue
     */
    public static ManyResult getTimesMany(int days, int... venueIds) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(HttpRequest.requestJson(getTimesManyUrl(days, venueIds)), ManyResult.class);
    }
}
