package uk.ac.tees.gingerbread.libcinelist.backend;

import okhttp3.*;

import java.io.IOException;
import java.net.URL;

class HttpRequest
{

    /**
     * Requests JSON using OkHttpClient
     *
     * @param url The request url
     * @return A string or null
     */
    static String requestJson(URL url)
    {
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/octet-stream");
        RequestBody body = RequestBody.create(mediaType, "{}");
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        try (Response response = client.newCall(request).execute())
        {
            assert response.body() != null;
            return response.body().string();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
