package uk.ac.tees.gingerbread.libcinelist.dto.get;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "title",
        "times"
})
public class Listing
{

    @JsonProperty("title")
    private String title;
    @JsonProperty("times")
    private List<String> times = null;

    @JsonProperty("title")
    public String getTitle()
    {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title)
    {
        this.title = title;
    }

    @JsonProperty("times")
    public List<String> getTimes()
    {
        return times;
    }

    @JsonProperty("times")
    public void setTimes(List<String> times)
    {
        this.times = times;
    }

}
