package uk.ac.tees.gingerbread.libfilmdb.backend;

import org.junit.Test;
import uk.ac.tees.gingerbread.libfilmdb.dto.search.SearchDto;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SearchRequestTest
{
    @Test
    public void searchNotNull() throws IOException
    {
        SearchDto dto = SearchRequest.getSearch("The Lego Movie 2", 2019);
        assertNotNull(dto);
    }

    @Test
    public void searchHasData() throws IOException
    {
        SearchDto dto = SearchRequest.getSearch("The Lego Movie 2", 2019);
        assertEquals(280217, dto.getResults()[0].getId());
    }
}