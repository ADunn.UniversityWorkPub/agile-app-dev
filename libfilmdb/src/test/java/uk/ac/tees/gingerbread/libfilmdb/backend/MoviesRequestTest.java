package uk.ac.tees.gingerbread.libfilmdb.backend;

import org.junit.Test;
import uk.ac.tees.gingerbread.libfilmdb.dto.movie.MovieDto;
import uk.ac.tees.gingerbread.libfilmdb.dto.movie.showing.NowShowingResultPage;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MoviesRequestTest
{

    //Is an object parsed?
    @Test
    public void movieNotNull() throws IOException
    {
        MovieDto movie = MoviesRequest.getDetails(280217);

        assertNotNull(movie);
    }

    //Does the data make sense?
    @Test
    public void movieHasProperData() throws IOException
    {
        MovieDto movie = MoviesRequest.getDetails(280217);

        assertEquals("The Lego Movie 2: The Second Part", movie.getTitle());
    }

    @Test
    public void hasNowShowingResults() throws IOException
    {
        List<NowShowingResultPage> movies = MoviesRequest.getNowShowingPages();
        assertNotNull(movies);
    }
}
