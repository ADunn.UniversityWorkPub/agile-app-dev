package uk.ac.tees.gingerbread.libfilmdb.backend;

import org.junit.Test;
import uk.ac.tees.gingerbread.libfilmdb.dto.config.ConfigurationDto;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ConfigurationRequestTest
{

    //Is the JSON parsed?
    @Test
    public void configHasValue() throws IOException
    {
        ConfigurationDto dto = ConfigurationRequest.getConfiguration();

        assertNotEquals(null, dto);
    }

    //Does the parsed object contain data?
    @Test
    public void configFieldsHaveValue() throws IOException
    {
        ConfigurationDto dto = ConfigurationRequest.getConfiguration();

        assertEquals("https://image.tmdb.org/t/p/", dto.getImages().getSecureBaseUrl());
    }

}