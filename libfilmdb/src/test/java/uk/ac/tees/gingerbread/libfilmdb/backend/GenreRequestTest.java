package uk.ac.tees.gingerbread.libfilmdb.backend;

import org.junit.Test;
import uk.ac.tees.gingerbread.libfilmdb.dto.genre.Genre;
import uk.ac.tees.gingerbread.libfilmdb.dto.genre.GenresDto;

import java.io.IOException;

import static org.junit.Assert.assertNotEquals;

public class GenreRequestTest
{

    //Is the JSON parsed?
    @Test
    public void TestNotNull() throws IOException
    {
        GenresDto list = GenreRequest.getGenreList();
        assertNotEquals(null, list);
    }

    //Does the parsed JSON actually contain data?
    @Test
    public void TestHasValue() throws IOException
    {
        GenresDto list = GenreRequest.getGenreList();

        Genre action = null;
        for (Genre g : list.getGenres())
        {
            if (g.getId().equals(28))
            {
                action = g;
            }
        }

        assertNotEquals(null, action);
    }
}
