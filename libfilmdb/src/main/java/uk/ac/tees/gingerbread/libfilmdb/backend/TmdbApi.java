package uk.ac.tees.gingerbread.libfilmdb.backend;

/**
 * Static data to be used by requests
 */
class TmdbApi
{
    private static final String API_URI = "https://api.themoviedb.org/3";
    private static final String API_KEY_V3 = "e1d8473aa1f0f28cdb3977a9bee7f711";
    private static final String API_LANG = "en-GB";

    /**
     * The v3 Api key
     *
     * @return Api key
     */
    static String getApiKey()
    {
        return API_KEY_V3;
    }

    /**
     * The API uri
     *
     * @return https://api.themoviedb.org/3
     */
    static String getApiUri()
    {
        return API_URI;
    }

    /**
     * The language to return requests in
     *
     * @return en-GB
     */
    static String getApiLang()
    {
        return API_LANG;
    }
}
