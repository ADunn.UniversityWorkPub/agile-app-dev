package uk.ac.tees.gingerbread.libfilmdb.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import uk.ac.tees.gingerbread.libfilmdb.dto.config.ConfigurationDto;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


public class ConfigurationRequest
{

    /**
     * Constructs a configuration request
     *
     * @return A URL for the request
     * @throws MalformedURLException Invalid url
     */
    private static URL getConfigurationURL() throws MalformedURLException
    {
        String url = TmdbApi.getApiUri() + "/configuration"
                     + "?api_key="
                     + TmdbApi.getApiKey();

        return new URL(url);
    }

    /**
     * Makes an API request for the server configuration
     *
     * @return An object representing the response
     * @throws IOException Malformed url or JSON parsing exception
     */
    public static ConfigurationDto getConfiguration() throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(getConfigurationURL(), ConfigurationDto.class);
    }
}
