package uk.ac.tees.gingerbread.libfilmdb.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import uk.ac.tees.gingerbread.libfilmdb.dto.genre.Genre;
import uk.ac.tees.gingerbread.libfilmdb.dto.genre.GenresDto;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class GenreRequest
{

    private static GenresDto CACHED_LIST;
    private static Map<Integer, Genre> CACHED_MAP;

    /**
     * Creates the url required to obtain a list of genre id-name pairs
     *
     * @return The url
     * @throws MalformedURLException Invalid Url
     */
    private static URL getGenreURL()
    {
        String url = TmdbApi.getApiUri() + "/genre/movie/list"
                     + "?api_key="
                     + TmdbApi.getApiKey()
                     + "&language="
                     + TmdbApi.getApiLang();

        try
        {
            return new URL(url);
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    private static void fetchGenres()
    {
        ObjectMapper mapper = new ObjectMapper();

        try
        {
            CACHED_LIST = mapper.readValue(HttpRequest.requestJson(getGenreURL()), GenresDto.class);
        }
        catch (IOException e)
        {
            e.printStackTrace();

        }

        CACHED_MAP = new HashMap<>();

        for (Genre g : CACHED_LIST.getGenres())
        {
            CACHED_MAP.put(g.getId(), g);
        }
    }

    /**
     * Returns a list of id-name pairs representing genres
     *
     * @return Id-name pair array
     * @throws IOException Malformed Url or JSON Parsing exception
     */
    static GenresDto getGenreList()
    {
        if (CACHED_LIST == null)
        {
            fetchGenres();
        }

        return CACHED_LIST;
    }

    /**
     * Gets the genre by the id
     *
     * @param id Id of the genre
     * @return The genre
     */
    public static Genre lookupId(int id)
    {
        if (CACHED_MAP == null)
        {
            fetchGenres();
        }

        return CACHED_MAP.get(id);
    }
}
