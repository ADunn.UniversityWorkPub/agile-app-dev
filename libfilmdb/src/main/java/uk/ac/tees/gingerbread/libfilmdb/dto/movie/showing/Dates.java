package uk.ac.tees.gingerbread.libfilmdb.dto.movie.showing;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "maximum",
        "minimum"
})
public class Dates
{

    @JsonProperty("maximum")
    private String maximum;
    @JsonProperty("minimum")
    private String minimum;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("maximum")
    public String getMaximum()
    {
        return maximum;
    }

    @JsonProperty("maximum")
    public void setMaximum(String maximum)
    {
        this.maximum = maximum;
    }

    @JsonProperty("minimum")
    public String getMinimum()
    {
        return minimum;
    }

    @JsonProperty("minimum")
    public void setMinimum(String minimum)
    {
        this.minimum = minimum;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties()
    {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value)
    {
        this.additionalProperties.put(name, value);
    }

}
