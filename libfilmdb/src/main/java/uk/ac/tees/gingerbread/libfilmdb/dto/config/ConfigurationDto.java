package uk.ac.tees.gingerbread.libfilmdb.dto.config;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API system config
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "images",
        "change_keys"
})
public class ConfigurationDto
{

    @JsonProperty("images")
    private Images images;
    @JsonProperty("change_keys")
    private List<String> changeKeys = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("images")
    public Images getImages()
    {
        return images;
    }

    @JsonProperty("images")
    public void setImages(Images images)
    {
        this.images = images;
    }

    @JsonProperty("change_keys")
    public List<String> getChangeKeys()
    {
        return changeKeys;
    }

    @JsonProperty("change_keys")
    public void setChangeKeys(List<String> changeKeys)
    {
        this.changeKeys = changeKeys;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties()
    {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value)
    {
        this.additionalProperties.put(name, value);
    }

}
