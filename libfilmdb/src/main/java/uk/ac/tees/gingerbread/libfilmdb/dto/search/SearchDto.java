package uk.ac.tees.gingerbread.libfilmdb.dto.search;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchDto
{
    private SearchResult[] results;

    public SearchResult[] getResults()
    {
        return results;
    }

    public void setResults(SearchResult[] results)
    {
        this.results = results;
    }
}
