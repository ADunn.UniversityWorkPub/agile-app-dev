package uk.ac.tees.gingerbread.libfilmdb.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import uk.ac.tees.gingerbread.libfilmdb.dto.movie.MovieDto;
import uk.ac.tees.gingerbread.libfilmdb.dto.movie.showing.NowShowingResultPage;
import uk.ac.tees.gingerbread.libfilmdb.dto.movie.showing.Result;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class MoviesRequest
{

    /**
     * Constructs a movie details request url
     *
     * @param movie_id The id of the movie
     * @return A URL of the request
     * @throws MalformedURLException Invalid url
     */
    private static URL getDetailsURL(int movie_id) throws MalformedURLException
    {
        String url = TmdbApi.getApiUri() + "/movie/" +
                     Integer.toString(movie_id) +
                     "?api_key=" +
                     TmdbApi.getApiKey() +
                     "&language=" +
                     TmdbApi.getApiLang();

        return new URL(url);
    }

    /**
     * Makes an API request for the details of a specific film
     *
     * @param movie_id The film id to get details for
     * @return A movie dto representing the response
     * @throws IOException Invalid URL or JSON Parsing issue
     */
    public static MovieDto getDetails(int movie_id) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(getDetailsURL(movie_id), MovieDto.class);
    }

    private static URL getNowShowingURL(int page) throws MalformedURLException
    {
        String url = TmdbApi.getApiUri() + "/movie/" +
                     "now_playing/" + "?api_key=" +
                     TmdbApi.getApiKey() +
                     "&language=" +
                     TmdbApi.getApiLang() +
                     "&region=GB" +
                     "&page=" + Integer.toString(page);

        return new URL(url);
    }

    /**
     * Returns all pages from the now showing request
     *
     * @return the returned pages containing all film results
     * @throws IOException Invalid url or json parsing issue
     */
    static List<NowShowingResultPage> getNowShowingPages() throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();
        NowShowingResultPage r1 = mapper.readValue(HttpRequest.requestJson(getNowShowingURL(1))
                , NowShowingResultPage.class);

        int numPages = r1.getTotalPages();

        List<NowShowingResultPage> pages = new ArrayList<>();
        pages.add(r1);
        for (int i = 2; i <= numPages; i++)
        {
            pages.add(mapper.readValue(HttpRequest.requestJson(getNowShowingURL(i)), NowShowingResultPage.class));
        }

        return pages;
    }

    /**
     * Returns all currently showing films
     *
     * @return A list of films
     * @throws IOException Invalid url or json parsing issue
     */
    public static List<Result> getNowShowingResults() throws IOException
    {
        List<NowShowingResultPage> pages = getNowShowingPages();
        List<Result> results = new ArrayList<>();

        for (NowShowingResultPage p : pages)
        {
            results.addAll(p.getResults());
        }

        return results;
    }
}
