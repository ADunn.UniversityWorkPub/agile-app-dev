package uk.ac.tees.gingerbread.libfilmdb.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import uk.ac.tees.gingerbread.libfilmdb.dto.search.SearchDto;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class SearchRequest
{

    /**
     * Constructs a new request url from the given query and year
     *
     * @param query The search term (not url encoded)
     * @param year  The year to filter by
     * @return A url of the request
     * @throws MalformedURLException Invalid url
     */
    private static URL getSearchURL(String query, int year) throws MalformedURLException, UnsupportedEncodingException
    {
        String url = TmdbApi.getApiUri() + "/search/"
                     + "movie?"
                     + "api_key="
                     + TmdbApi.getApiKey()
                     + "&language="
                     + TmdbApi.getApiLang()
                     + "&query="
                     + URLEncoder.encode(query, "UTF-8")
                     + "&include_adult=false"
                     + "&region=GB"
                     + "&primary_release_year="
                     + Integer.toString(year);

        return new URL(url);
    }

    /**
     * Makes an API request to search for a film
     *
     * @param query The search term
     * @param year  The year to filter by
     * @return A search object representing the response
     * @throws IOException A Malformed URL or parsing exception
     */
    public static SearchDto getSearch(String query, int year) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(getSearchURL(query, year), SearchDto.class);
    }
}
