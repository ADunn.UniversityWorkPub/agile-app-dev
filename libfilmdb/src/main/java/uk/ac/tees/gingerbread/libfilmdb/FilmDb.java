package uk.ac.tees.gingerbread.libfilmdb;

import uk.ac.tees.gingerbread.libfilmdb.backend.ConfigurationRequest;
import uk.ac.tees.gingerbread.libfilmdb.backend.MoviesRequest;
import uk.ac.tees.gingerbread.libfilmdb.backend.SearchRequest;
import uk.ac.tees.gingerbread.libfilmdb.dto.movie.MovieDto;
import uk.ac.tees.gingerbread.libfilmdb.dto.movie.showing.Result;
import uk.ac.tees.gingerbread.libfilmdb.dto.search.SearchDto;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class FilmDb
{
    /**
     * Attempts to find a specific film
     *
     * @param query The name of the film
     * @param year  The year of release to filter by
     * @return A film or null if it couldn't be found
     */
    public static Movie findFilm(String query, int year)
    {
        try
        {
            SearchDto result = SearchRequest.getSearch(query, year);

            if (result.getResults().length == 0)
            {
                return null;
            }
            else
            {
                int filmId = result.getResults()[0].getId();

                MovieDto dto = MoviesRequest.getDetails(filmId);
                return Movie.fromDto(dto);
            }

        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Gets the url to a movie's poster
     *
     * @param movie The movie to get the poster for
     * @return The url
     * @throws IOException A url parsing exception
     */
    public static URL getPosterUrl(Movie movie) throws IOException
    {
        String base = ConfigurationRequest.getConfiguration().getImages().getSecureBaseUrl();
        base += "w500" + movie.getPosterPath();

        return new URL(base);
    }

    /**
     * Returns all currently showing movies
     *
     * @return All currently showing movies
     * @throws IOException Http request error or json parsing issue
     */
    public static List<Movie> getNowShowingMovies() throws IOException
    {
        List<Result> results = MoviesRequest.getNowShowingResults();

        List<Movie> list = new ArrayList<>();
        for (Result result : results)
        {
            Movie movie = Movie.fromResult(result);
            list.add(movie);
        }
        return list;
    }
}
