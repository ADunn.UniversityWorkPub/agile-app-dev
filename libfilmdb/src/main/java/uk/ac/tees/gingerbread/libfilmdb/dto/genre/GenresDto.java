package uk.ac.tees.gingerbread.libfilmdb.dto.genre;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

/**
 * DTO list of all genres
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "genres"
})
public class GenresDto
{

    @JsonProperty("genres")
    private List<Genre> genres = null;

    @JsonProperty("genres")
    public List<Genre> getGenres()
    {
        return genres;
    }

    @JsonProperty("genres")
    public void setGenres(List<Genre> genres)
    {
        this.genres = genres;
    }

}
