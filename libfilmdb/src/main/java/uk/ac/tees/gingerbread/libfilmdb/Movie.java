package uk.ac.tees.gingerbread.libfilmdb;

import uk.ac.tees.gingerbread.libfilmdb.backend.GenreRequest;
import uk.ac.tees.gingerbread.libfilmdb.dto.genre.Genre;
import uk.ac.tees.gingerbread.libfilmdb.dto.movie.MovieDto;
import uk.ac.tees.gingerbread.libfilmdb.dto.movie.showing.Result;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import static java.util.Calendar.*;

public class Movie implements Serializable
{
    private int id;
    private String title;
    private String posterPath;
    private Genre[] genres;
    private String overview;
    private Calendar releaseDate;

    //TODO Add data
    private String studio;
    private String rating;

    static Movie fromDto(MovieDto dto)
    {
        Movie movie = new Movie();

        movie.setId(dto.getId());
        movie.setTitle(dto.getTitle());
        movie.setPosterPath(dto.getPosterPath());
        movie.setGenres((Genre[]) dto.getGenres().toArray());
        movie.setOverview(dto.getOverview());

        //Convert ISO date to calendar object
        String[] fields = dto.getReleaseDate().split("-");

        Calendar n = new GregorianCalendar();
        n.set(YEAR, Integer.parseInt(fields[0]));
        n.set(MONTH, Integer.parseInt(fields[1]));
        n.set(DAY_OF_MONTH, Integer.parseInt(fields[2]));

        movie.setReleaseDate(n);

        return movie;
    }

    static Movie fromResult(Result dto)
    {
        Movie movie = new Movie();

        movie.setId(dto.getId());
        movie.setTitle(dto.getTitle());
        movie.setPosterPath(dto.getPosterPath());
        movie.setOverview(dto.getOverview());

        //Convert ISO date to calendar object
        String[] fields = dto.getReleaseDate().split("-");

        Calendar n = new GregorianCalendar();
        n.set(YEAR, Integer.parseInt(fields[0]));
        n.set(MONTH, Integer.parseInt(fields[1]));
        n.set(DAY_OF_MONTH, Integer.parseInt(fields[2]));

        movie.setReleaseDate(n);

        List<Genre> genres = new ArrayList<>();

        for (Integer i : dto.getGenreIds())
        {
            genres.add(GenreRequest.lookupId(i));
        }

        movie.setGenres(genres.toArray(new Genre[0]));
        return movie;
    }

    public int getId()
    {
        return id;
    }

    private void setId(int id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    private void setTitle(String title)
    {
        this.title = title;
    }

    public String getPosterPath()
    {
        return posterPath;
    }

    private void setPosterPath(String posterPath)
    {
        this.posterPath = posterPath;
    }

    public Genre[] getGenres()
    {
        return genres;
    }

    private void setGenres(Genre[] genres)
    {
        this.genres = genres;
    }

    public String getOverview()
    {
        return overview;
    }

    private void setOverview(String overview)
    {
        this.overview = overview;
    }

    public Calendar getReleaseDate()
    {
        return releaseDate;
    }

    private void setReleaseDate(Calendar releaseDate)
    {
        this.releaseDate = releaseDate;
    }

    public String getStudio()
    {
        return studio;
    }

    public void setStudio(String studio)
    {
        this.studio = studio;
    }

    public String getRating()
    {
        return rating;
    }

    public void setRating(String rating)
    {
        this.rating = rating;
    }
}
