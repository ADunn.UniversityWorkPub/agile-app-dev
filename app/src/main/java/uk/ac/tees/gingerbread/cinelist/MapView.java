package uk.ac.tees.gingerbread.cinelist;

import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.SparseArray;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import uk.ac.tees.gingerbread.cinelist.background.AsyncNetwork;
import uk.ac.tees.gingerbread.cinelist.data.GnssLocation;
import uk.ac.tees.gingerbread.cinelist.data.Location;
import uk.ac.tees.gingerbread.cinelist.data.PostcodeLocation;
import uk.ac.tees.gingerbread.libcinelist.dto.get.CinemaDetails;
import uk.ac.tees.gingerbread.libcinelist.dto.get.Listing;
import uk.ac.tees.gingerbread.libcinelist.dto.get.ManyResult;
import uk.ac.tees.gingerbread.libcinelist.dto.get.Result;
import uk.ac.tees.gingerbread.libfilmdb.Movie;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MapView extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener
{

    private GoogleMap mMap;
    private Movie movie;
    private Location location;
    private SparseArray<CinemaDetails> cinemas;
    private Address address;
    private AsyncTask<Integer, Integer, ManyResult> showings;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_view);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        movie = (Movie) getIntent().getExtras().getSerializable("movie");
        location = (Location) getIntent().getExtras().getSerializable("location");

        try
        {
            cinemas = AsyncNetwork.getCinemas(location).get();

            Integer[] venues = new Integer[cinemas.size()];
            for (int i = 0; i < cinemas.size(); i++) {
                venues[i] = cinemas.keyAt(i);
            }

            showings = AsyncNetwork.getManyShowings(0, venues);

        }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }

        if (location instanceof PostcodeLocation)
        {
            Geocoder geocoder = new Geocoder(getApplicationContext());

            try
            {
                address = geocoder.getFromLocationName(((PostcodeLocation) location).getPostcode(), 1).get(0);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;

        mMap.setOnMarkerClickListener(this);

        //Add marker for user location
        LatLng userLoc;

        if (location instanceof PostcodeLocation)
        {
            userLoc = new LatLng(address.getLatitude(), address.getLongitude());
        }
        else
        {
            userLoc = new LatLng(((GnssLocation) location).getLatitude(), ((GnssLocation) location).getLongitude());
        }

        mMap.addMarker(new MarkerOptions()
                               .position(userLoc)
                               .title("Your Location")
                               .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        mMap.moveCamera(CameraUpdateFactory
                                .newLatLngZoom(userLoc, 10));

        //Add markers for cinemas
        for (int i = 0; i < cinemas.size(); i++)
        {
            int id = cinemas.keyAt(i);
            CinemaDetails c = cinemas.valueAt(i);

            Marker m = mMap.addMarker(getMarker(c));
            m.setTag(c);
            Listing l = findFilm(movie.getTitle(), getListingsFromId(id));

            StringBuilder snippet = new StringBuilder("Showings:\n");

            if (l != null)
            {
                for (int j = 0; j < l.getTimes().size(); j++)
                {
                    snippet.append(l.getTimes().get(j))
                           .append("\n");
                }
            }
            m.setSnippet(snippet.toString());
        }
    }

    private MarkerOptions getMarker(CinemaDetails cinemaDetails)
    {
        return new MarkerOptions()
                .position(new LatLng(Double.parseDouble(cinemaDetails.getLat()),
                                     Double.parseDouble(cinemaDetails.getLon())))
                .title(cinemaDetails.getName())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
    }

    @Override
    public boolean onMarkerClick(Marker marker)
    {
        return false;
    }

    private List<Listing> getListingsFromId(int venueId) {
        try
        {
            for (Result r: showings.get().getResults()) {
                if (Integer.parseInt(r.getCinema()) == venueId) {
                    return r.getListings();
                }
            }
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        catch (ExecutionException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    private Listing findFilm(String filmName, List<Listing> listings) {
        for (Listing l : listings) {
            if (l.getTitle().equals(filmName)) {
                return l;
            }
        }

        return null;
    }
}
