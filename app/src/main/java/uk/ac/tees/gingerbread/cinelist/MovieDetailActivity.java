package uk.ac.tees.gingerbread.cinelist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import uk.ac.tees.gingerbread.cinelist.background.AsyncNetwork;
import uk.ac.tees.gingerbread.libfilmdb.Movie;

import java.util.Objects;

public class MovieDetailActivity extends AppCompatActivity
{

    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        movie = (Movie) Objects.requireNonNull(getIntent().getExtras()).getSerializable("movie");

        ImageView movieThumbnailImg = findViewById(R.id.detail_movie_img);
        try
        {
            movieThumbnailImg.setImageBitmap(AsyncNetwork.getMovieThumbnail(movie).get());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        ((TextView) findViewById(R.id.detail_movie_title)).setText(movie.getTitle());

        TextView desc = findViewById(R.id.detail_movie_desc);
        desc.setText(movie.getOverview());
        desc.setMovementMethod(new ScrollingMovementMethod());
    }

    public void onShowingsClick(View view)
    {
        Intent loc = new Intent(this, LocationEntry.class);
        loc.putExtra("movie", movie);
        startActivity(loc);
    }
}
