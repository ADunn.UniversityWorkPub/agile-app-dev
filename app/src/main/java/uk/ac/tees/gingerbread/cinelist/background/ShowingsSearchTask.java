package uk.ac.tees.gingerbread.cinelist.background;

import android.os.AsyncTask;
import uk.ac.tees.gingerbread.libcinelist.Cinelist;
import uk.ac.tees.gingerbread.libcinelist.dto.get.ManyResult;

import java.io.IOException;

class ShowingsSearchTask extends AsyncTask<Integer, Integer, ManyResult>
{
    private int dayOffset;

    ShowingsSearchTask(int dayOffset)
    {
        this.dayOffset = dayOffset;
    }

    @Override
    protected ManyResult doInBackground(Integer... integers)
    {
        int[] unbox = new int[integers.length];

        for (int i = 0; i < integers.length; i++)
        {
            unbox[i] = integers[i];
        }

        try
        {
            return Cinelist.getManyShowings(dayOffset, unbox);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
