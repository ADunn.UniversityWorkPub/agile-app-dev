package uk.ac.tees.gingerbread.cinelist.background;

import android.os.AsyncTask;
import uk.ac.tees.gingerbread.libfilmdb.FilmDb;
import uk.ac.tees.gingerbread.libfilmdb.Movie;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class LatestFilmsTask extends AsyncTask<Integer, Integer, List<Movie>>
{
    @Override
    protected List<Movie> doInBackground(Integer... integers)

    {
        List<Movie> movies = new ArrayList<>();

        try
        {
            movies = FilmDb.getNowShowingMovies();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return movies;
    }
}
