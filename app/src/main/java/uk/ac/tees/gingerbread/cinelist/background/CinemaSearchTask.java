package uk.ac.tees.gingerbread.cinelist.background;

import android.os.AsyncTask;
import android.util.SparseArray;
import uk.ac.tees.gingerbread.cinelist.data.GnssLocation;
import uk.ac.tees.gingerbread.cinelist.data.Location;
import uk.ac.tees.gingerbread.cinelist.data.PostcodeLocation;
import uk.ac.tees.gingerbread.libcinelist.Cinelist;
import uk.ac.tees.gingerbread.libcinelist.dto.get.CinemaDetails;
import uk.ac.tees.gingerbread.libcinelist.dto.search.Cinema;

import java.io.IOException;
import java.util.*;

class CinemaSearchTask extends AsyncTask<Location, Integer, SparseArray<CinemaDetails>>
{
    @Override
    protected SparseArray<CinemaDetails> doInBackground(Location... locations)
    {
        Location loc = locations[0];
        List<Cinema> cinemas = new ArrayList<>();

        try
        {
            if (loc instanceof PostcodeLocation)
            {


                cinemas = Cinelist.getCinemasByPostcode(((PostcodeLocation) loc).getPostcode()).getCinemas();

            }
            else if (loc instanceof GnssLocation)
            {
                cinemas = Cinelist
                        .getCinemasByLatLong(((GnssLocation) loc).getLatitude(),
                                             ((GnssLocation) loc).getLongitude())
                        .getCinemas();
            }

            SparseArray<CinemaDetails> array = new SparseArray<>();

            for (Cinema c : cinemas)
            {
                array.append(Integer.parseInt(c.getId()), Cinelist.getCinemaDetails(Integer.parseInt(c.getId())));
            }

            return array;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
