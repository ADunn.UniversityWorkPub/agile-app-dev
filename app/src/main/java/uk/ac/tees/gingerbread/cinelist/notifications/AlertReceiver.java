package uk.ac.tees.gingerbread.cinelist.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class AlertReceiver extends BroadcastReceiver
{


    /* Responsible for handling code after a broadcast has been sent based on
     * the calendar time set in the Home Activity class (default 12:00pm every day).
     * It is known not to be accurate at times and could have delays in seconds or
     * even minutes.*/

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationHelper notificationHelper = new NotificationHelper(context);
        NotificationCompat.Builder nb = notificationHelper.getChannelNotification();

        //Sends the notification to the screen
        notificationHelper.getManager().notify(1, nb.build());
    }
}
