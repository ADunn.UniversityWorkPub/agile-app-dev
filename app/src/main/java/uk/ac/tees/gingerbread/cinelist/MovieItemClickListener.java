package uk.ac.tees.gingerbread.cinelist;

import android.widget.ImageView;
import uk.ac.tees.gingerbread.libfilmdb.Movie;

public interface MovieItemClickListener
{

    void onMovieClick(Movie movie, ImageView movieImageView);
}
