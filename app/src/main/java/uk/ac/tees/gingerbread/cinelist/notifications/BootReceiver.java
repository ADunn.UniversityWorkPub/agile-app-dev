package uk.ac.tees.gingerbread.cinelist.notifications;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

public class BootReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        /* NOTIFICATIONS (REUSED) */

        //Time at which the notification should appear on screen
        //There could be potential time delays
        Calendar c = Calendar.getInstance();

        //Setting a FIXED time for a daily notification (12:00pm)
        c.set(Calendar.HOUR_OF_DAY, 12);
        c.set(Calendar.MINUTE, 00);
        c.set(Calendar.SECOND, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        //Responsible for broadcasting the message to the AlertReceiver (Broadcaster Service)
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1, intent, 0);

        /* This check makes sure that if the time set for the alarm
         * is earlier than the current time, it will be skipped */
        if (c.before(Calendar.getInstance())) {
            c.add(Calendar.DATE, 1);
        }

        //Makes sure that the notification is also set for the following day after broadcasting
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    }
}
