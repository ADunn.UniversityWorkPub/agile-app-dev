package uk.ac.tees.gingerbread.cinelist.data;

import android.location.Geocoder;

/**
 * Stores a location from a postcode
 */
public class PostcodeLocation extends Location
{
    private String postcode;

    public PostcodeLocation(String postcode)
    {
        this.postcode = postcode;
    }

    public String getPostcode()
    {
        return postcode;
    }

    public void setPostcode(String postcode)
    {
        this.postcode = postcode;
    }
}
