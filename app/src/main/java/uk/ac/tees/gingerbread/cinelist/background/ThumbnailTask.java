package uk.ac.tees.gingerbread.cinelist.background;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import uk.ac.tees.gingerbread.libfilmdb.FilmDb;
import uk.ac.tees.gingerbread.libfilmdb.Movie;

import java.io.IOException;

public class ThumbnailTask extends AsyncTask<Movie, Integer, Bitmap>
{
    @Override
    protected Bitmap doInBackground(Movie... movies)
    {
        Bitmap result = null;

        try
        {
            result = BitmapFactory.decodeStream(FilmDb.getPosterUrl(movies[0]).openStream());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return result;
    }
}
