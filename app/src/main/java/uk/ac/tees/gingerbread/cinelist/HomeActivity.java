package uk.ac.tees.gingerbread.cinelist;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.ImageView;
import uk.ac.tees.gingerbread.cinelist.background.AsyncNetwork;
import uk.ac.tees.gingerbread.cinelist.list.activities.ListFunction;
import uk.ac.tees.gingerbread.cinelist.notifications.AlertReceiver;
import uk.ac.tees.gingerbread.libfilmdb.Movie;

import java.util.*;

public class HomeActivity extends AppCompatActivity implements MovieItemClickListener
{

    private List<Slide> lstSlides;
    private ViewPager sliderpager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        sliderpager = findViewById(R.id.slider_pager);
        TabLayout indicator = findViewById(R.id.indicator);
        RecyclerView moviesRV = findViewById(R.id.Rv_movies);
        Button showings = findViewById(R.id.showings);

        showings.setOnClickListener(v -> openFilmShowings());

        lstSlides = new ArrayList<>();
        lstSlides.add(new Slide(R.drawable.slide1, "Aladdin \nComing May 24th"));
        lstSlides.add(new Slide(R.drawable.slide2, "John Wick 3: Parabellum \nComing May 15th"));
        lstSlides.add(new Slide(R.drawable.slide3, "The Hustle \nComing May 10th"));
        lstSlides.add(new Slide(R.drawable.slide4, "Ad Astra \nComing May 24th"));
        SliderPagerAdapter adapter = new SliderPagerAdapter(this, lstSlides);
        sliderpager.setAdapter(adapter);
        Timer timer = new Timer();

        timer.scheduleAtFixedRate(new HomeActivity.SliderTimer(this), 4000, 6000);

        indicator.setupWithViewPager(sliderpager, true);

        MovieAdapter movieAdapter = null;

        try
        {
            movieAdapter = new MovieAdapter(this, AsyncNetwork.getLatestMovies().get(), this);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        moviesRV.setAdapter(movieAdapter);
        moviesRV.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }

    public void openFilmShowings()
    {
        Intent intent = new Intent(this, ListFunction.class);
        startActivity(intent);
    }

    @Override
    public void onMovieClick(Movie movie, ImageView movieImageView)
    {

        Intent intent = new Intent(this, MovieDetailActivity.class);
        intent.putExtra("movie", movie);
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(HomeActivity.this,
                                                                               movieImageView, "sharedName");
        startActivity(intent, options.toBundle());

        //Toast.makeText(this, "item clicked : " + movie.getTitle(), Toast.LENGTH_LONG).show();
    }

    class SliderTimer extends TimerTask
    {
        private Activity activity;

        SliderTimer(Activity activity) {
            this.activity = activity;
        }

        @Override
        public void run()
        {

            HomeActivity.this.runOnUiThread(() ->
                                            {
                                                if (sliderpager.getCurrentItem() < lstSlides.size() - 1)
                                                {
                                                    sliderpager.setCurrentItem(sliderpager.getCurrentItem() + 1);
                                                }
                                                else
                                                {
                                                    sliderpager.setCurrentItem(0);
                                                }
                                            });
        /* NOTIFICATIONS */

        //Time at which the notification should appear on screen
        //There could be potential time delays
        Calendar c = Calendar.getInstance();

        //Setting a FIXED time for a daily notification (12:00pm)
        c.set(Calendar.HOUR_OF_DAY, 12);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);

        //Calls for the alarm to start
        startAlarm(c);
        }

    private void startAlarm(Calendar c) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        //Responsible for broadcasting the message to the AlertReceiver (Broadcaster Service)
        Intent intent = new Intent(activity, AlertReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(activity, 1, intent, 0);

        /* This check makes sure that if the time set for the alarm
         * is earlier than the current time, it will be skipped */
        if (c.before(Calendar.getInstance())) {
            c.add(Calendar.DATE, 1);
        }

        //Makes sure that the notification is also set for the following day after broadcasting
        if (alarmManager != null)
        {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }

    //Could be used for stopping the notifications in the application's settings
    /*private void cancelAlarm() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlertReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);

        alarmManager.cancel(pendingIntent);
    }*/
    }

}
