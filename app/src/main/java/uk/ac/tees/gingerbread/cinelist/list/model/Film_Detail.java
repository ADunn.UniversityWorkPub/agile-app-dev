package uk.ac.tees.gingerbread.cinelist.list.model;

public class Film_Detail
{

    private String name;
    private String Description;
    private String rating;
    private String category;
    private String image_url;

    public Film_Detail()
    {
    }

    public Film_Detail(String name, String description, String rating, String category, String image_url)
    {
        this.name = name;
        Description = description;
        this.rating = rating;
        this.category = category;
        this.image_url = image_url;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return Description;
    }

    public void setDescription(String description)
    {
        Description = description;
    }

    public String getRating()
    {
        return rating;
    }

    public void setRating(String rating)
    {
        this.rating = rating;
    }

    public String getCategory()
    {
        return category;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }

    public String getImage_url()
    {
        return image_url;
    }

    public void setImage_url(String image_url)
    {
        this.image_url = image_url;
    }
}