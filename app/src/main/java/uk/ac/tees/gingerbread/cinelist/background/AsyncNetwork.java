package uk.ac.tees.gingerbread.cinelist.background;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.SparseArray;
import uk.ac.tees.gingerbread.cinelist.data.Location;
import uk.ac.tees.gingerbread.libcinelist.dto.get.CinemaDetails;
import uk.ac.tees.gingerbread.libcinelist.dto.get.ManyResult;
import uk.ac.tees.gingerbread.libfilmdb.Movie;

import java.util.List;

public class AsyncNetwork
{
    /**
     * Gets a film's poster image in the background
     *
     * @param movie The movie to get an image for
     * @return The image as a decoded bitmap
     */
    public static AsyncTask<Movie, Integer, Bitmap> getMovieThumbnail(Movie movie)
    {
        return new ThumbnailTask().execute(movie);
    }

    /**
     * Get a list of the latest films
     *
     * @return A list of the latest films
     */
    public static AsyncTask<Integer, Integer, List<Movie>> getLatestMovies()
    {
        return new LatestFilmsTask().execute();
    }

    /**
     * Gets a list of cinemas near a location
     *
     * @param location Either a GnssLocation or a PostcodeLocation
     * @return A list of cinemas w/ venueIds
     */
    public static AsyncTask<Location, Integer, SparseArray<CinemaDetails>> getCinemas(Location location)
    {
        return new CinemaSearchTask().execute(location);
    }

    /**
     * Gets showings at one or more cinemas
     *
     * @param dayOffset The number of days into the future to get showings for
     * @param venues    The venues to get showings for
     * @return A list of showings
     */
    public static AsyncTask<Integer, Integer, ManyResult> getManyShowings(int dayOffset, Integer... venues)
    {
        return new ShowingsSearchTask(dayOffset).execute(venues);
    }
}
