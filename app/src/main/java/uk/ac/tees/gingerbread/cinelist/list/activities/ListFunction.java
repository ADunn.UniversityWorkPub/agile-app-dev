package uk.ac.tees.gingerbread.cinelist.list.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import uk.ac.tees.gingerbread.cinelist.list.adapters.ViewAdapter;
import uk.ac.tees.gingerbread.cinelist.list.model.Film_Detail;
import uk.ac.tees.gingerbread.cinelist.R;

import java.util.ArrayList;
import java.util.List;

public class ListFunction extends AppCompatActivity
{

    private final String JSON_URL = ""; //insert IMDB latest showings JSON link
    private JsonArrayRequest request;
    private RequestQueue requestQueue;
    private List<Film_Detail> theFilm;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_main);

        theFilm = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerviewid);
        jsonrequest();
    }

    private void jsonrequest()
    {

        request = new JsonArrayRequest(JSON_URL, new Response.Listener<JSONArray>()
        {
            @Override
            public void onResponse(JSONArray response)
            {

                JSONObject jsonObject = null;

                for (int i = 0; i < response.length(); i++)
                {

                    try
                    {
                        jsonObject = response.getJSONObject(i);
                        Film_Detail film = new Film_Detail();
                        film.setName(jsonObject.getString("name"));
                        film.setDescription(jsonObject.getString("description"));
                        film.setRating(jsonObject.getString("Rating"));
                        film.setCategory(jsonObject.getString("category"));
                        film.setImage_url(jsonObject.getString("img"));
                        theFilm.add(film);

                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }

                setuprecyclerview(theFilm);

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {

            }
        });

        requestQueue = Volley.newRequestQueue(ListFunction.this);
        requestQueue.add(request);
    }

    private void setuprecyclerview(List<Film_Detail> theFilm)
    {
        ViewAdapter connection = new ViewAdapter(this, theFilm);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(connection);
    }
}