package uk.ac.tees.gingerbread.cinelist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import uk.ac.tees.gingerbread.cinelist.background.AsyncNetwork;
import uk.ac.tees.gingerbread.libfilmdb.Movie;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder>
{

    private Context context;
    private List<Movie> mData;
    private MovieItemClickListener movieItemClickListener;

    MovieAdapter(Context context, List<Movie> mData, MovieItemClickListener listener)
    {
        this.context = context;
        this.mData = mData;
        movieItemClickListener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.movie_items, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i)
    {
        myViewHolder.TvTitle.setText(mData.get(i).getTitle());
        try {
            myViewHolder.ImgMovie.setImageBitmap(AsyncNetwork.getMovieThumbnail(mData.get(i)).get());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount()
    {
        return mData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {

        private TextView TvTitle;
        private ImageView ImgMovie;

        MyViewHolder(@NonNull View itemView)
        {

            super(itemView);
            TvTitle = itemView.findViewById(R.id.item_movie_title);
            ImgMovie = itemView.findViewById(R.id.item_movie_img);

            itemView.setOnClickListener(
                    v -> movieItemClickListener.onMovieClick(mData.get(getAdapterPosition()), ImgMovie));
        }
    }
}
