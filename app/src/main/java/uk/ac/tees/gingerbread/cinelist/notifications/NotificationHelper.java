package uk.ac.tees.gingerbread.cinelist.notifications;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import uk.ac.tees.gingerbread.cinelist.R;

import java.util.Random;

public class NotificationHelper extends ContextWrapper
{
    public static final String CHANNEL_ID = "main_channel";
    public static final String CHANNEL_NAME = "Main Channel";

    private NotificationManager nm;

    //Could use an enum for a list of different notifications
    //Used for the purposes of the notification content
    Random random = new Random();
    String[] texts = {"New movie just came out!",
                      "Check out this new movie!",
                      "Have you rated this yet?",
                      "Check the closest cinema to you!"};

    public NotificationHelper(Context base) {
        super(base);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Creates channels necessary to work on API > 26 (Oreo)
            createChannel();
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createChannel() {
        /* There can be many different importance/priority levels:

                      -DEFAULT = show, sound, don't visually intrude into the screen
                      -HIGH = show, noise and peek
                      -LOW = show, not intrusive
                      -MIN = only pop up in the tray
                      -NONE = won't show up */

        NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);

        getManager().createNotificationChannel(channel);
    }

    public NotificationManager getManager() {
        if (nm == null) {
            nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }

        return nm;
    }

    public NotificationCompat.Builder getChannelNotification() {
        return new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID) //Return notification on the set channel
                .setContentTitle("Cinelist")
                .setContentText(texts[0 + random.nextInt(3 - 0 + 1)]) //Chooses a random number out of the array
                .setSmallIcon(R.drawable.ic_launcher_background); //The icon must be set, otherwise it crashes
    }
}
