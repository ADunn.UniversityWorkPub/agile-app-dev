package uk.ac.tees.gingerbread.cinelist.list.activities;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import uk.ac.tees.gingerbread.cinelist.R;

public class FilmActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film);

        getSupportActionBar().hide();

        String name = getIntent().getExtras().getString("movie_name");
        String description = getIntent().getExtras().getString("movie_description");
        String category = getIntent().getExtras().getString("movie_category");
        String rating = getIntent().getExtras().getString("movie_rating");
        String image_url = getIntent().getExtras().getString("movie_img");

        CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapsingtoolbar_id);
        collapsingToolbarLayout.setTitleEnabled(true);

        TextView tv_name = findViewById(R.id.f_movie_name);
        TextView tv_category = findViewById(R.id.f_category);
        TextView tv_description = findViewById(R.id.f_description);
        TextView tv_rating = findViewById(R.id.f_rating);
        ImageView img = findViewById(R.id.f_thumbnail);

        tv_name.setText(name);
        tv_category.setText(category);
        tv_description.setText(description);
        tv_rating.setText(rating);
        collapsingToolbarLayout.setTitle(name);

        RequestOptions requestOptions = new RequestOptions().centerCrop().placeholder(R.drawable.loading_shape)
                                                            .error(R.drawable.loading_shape);
        Glide.with(this).load(image_url).apply(requestOptions).into(img);
    }
}