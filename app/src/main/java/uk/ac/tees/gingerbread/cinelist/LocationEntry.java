package uk.ac.tees.gingerbread.cinelist;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.Task;
import uk.ac.tees.gingerbread.cinelist.data.GnssLocation;
import uk.ac.tees.gingerbread.cinelist.data.PostcodeLocation;
import uk.ac.tees.gingerbread.libfilmdb.Movie;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LocationEntry extends AppCompatActivity
{
    private static final int PERMISSION_LOCATION = 1;

    private FusedLocationProviderClient locationClient;
    private LocationRequest locationRequest;
    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_entry);

        movie = (Movie) getIntent().getExtras().getSerializable("movie");

        locationClient = LocationServices.getFusedLocationProviderClient(this);

        LocationRequest request = LocationRequest.create();
        request.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    /**
     * Get Lat / Long and pass to next activity
     *
     * @param view The current view
     */
    public void submitUsingGnss(View view)
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
            PackageManager.PERMISSION_DENIED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                              PERMISSION_LOCATION);
        }
        else
        {
            startLocationRequest();
        }

    }

    public void submitUsingPostcode(View view)
    {
        EditText postcodeEntry = findViewById(R.id.postcodeEntry);
        String postcode = postcodeEntry.getText().toString();

        if (validatePostcode(postcode))
        {
            PostcodeLocation loc = new PostcodeLocation(postcode.toUpperCase());
            //Toast.makeText(getApplicationContext(), "DEBUG: " + loc.getPostcode(), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, MapView.class)
                                  .putExtra("movie", movie)
                                  .putExtra("location", loc));
        }
        else
        {
            Toast toast = Toast.makeText(getApplicationContext(), R.string.location_entry_postcode_error_toast,
                                         Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

    }

    /**
     * Validates a string against a regex provided by gov.uk
     *
     * @param postcode The string to test
     * @return True if the text is a postcode
     */
    private boolean validatePostcode(String postcode)
    {
        //Regex from https://webarchive.nationalarchives.gov.uk/+/http://www.cabinetoffice.gov.uk/media/291370/bs7666-v2-0-xsd-PostCodeType.htm
        Pattern regex = Pattern.compile(
                "^(([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\\s?[0-9][A-Za-z]{2}))$");
        Matcher matcher = regex.matcher(postcode.toUpperCase());
        return matcher.matches();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        switch (requestCode)
        {
            case PERMISSION_LOCATION:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    startLocationRequest();
                }
                else
                {
                    findViewById(R.id.labelLocationError).setVisibility(View.VISIBLE);
                }
            }
        }
    }

    /**
     * Starts fetching the last location
     */
    //Suppress Warning as checked elsewhere
    @SuppressLint("MissingPermission")
    private void startLocationRequest()
    {
        TextView locationError = findViewById(R.id.labelLocationError);

        locationError.setVisibility(View.INVISIBLE);
        Task<Location> location = locationClient.getLastLocation();

        location.addOnSuccessListener(l ->
                                      {
                                          if (l != null)
                                          {
                                              GnssLocation loc = new GnssLocation(l.getLatitude(),
                                                                                  l.getLongitude());
                                              Toast.makeText(getApplicationContext(),
                                                             "DEBUG: lat=" + loc.getLatitude() + ",long=" + loc
                                                                     .getLongitude(), Toast.LENGTH_SHORT).show();
                                              //TODO Call next activity, passing in GnssLocation loc
                                          }
                                      });

        location.addOnFailureListener(l -> locationError.setVisibility(View.VISIBLE));
    }
}
